<div id="holder">
    <?php
    echo $this->beforeWidget;
    ?>
    
    <h1><?php echo $this->widgetTitle; ?></h1>
    <?php
    foreach($this->templateData as $key => $value):
    ?>
    <div class="customPost">
        <p class="customPostTitle"><?php echo $value['title']; ?></p>
        <p class="customPostContent"><?php echo $value['content']; ?></p>
        <?php echo ($value['image'] !== null ? $value['image'] : '');?>
    </div>
    <?php
    if($key != (count($this->templateData)-1))
        echo '<hr>';
    endforeach;
    echo $this->afterWidget;
    ?>
</div>