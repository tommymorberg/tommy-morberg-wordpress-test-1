<?php
require_once(__DIR__ . '/class/view.php');
require_once(__DIR__ . '/class/class-custom-post.php');
require_once(__DIR__ . '/class/class-widget.php');
/*
Plugin Name: Lexicon IT-Konsult: WordPress Test 1
Description: WordPress Test 1
Author: Lexicon IT-Konsult
Version: 1.0
*/
$customPost = new LexiconWpTest1CustomPost();
$customPost->init();