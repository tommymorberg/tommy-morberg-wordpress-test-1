<?php

/*
 * Used for showing a template file, in this case another php-file
 */
class View
{
    /*
     * The class I made to remove some html from the widget php file
     * @category    Extra
     * @package     Extra_test
     * @subpackage  Test
     * @copyright   Copyright (c) 2015 Tommy Morberg
     * @license     Test
     * @version     0.2
     * @link        http://google.com
     * @since       Class available since 0.1
     * @deprecated  Never
     */
    protected $templateDir = "\\templates\\";
    /*
     * Construct function
     * Setting variable $templateDir
     */
    public function __construct($pTemplateDir = null)
    {
        if($pTemplateDir !== null)
            $this->templateDir = $pTemplateDir;
    }
    
    /*
     * Renders a file, link to file passed as argument
     * Casts exception if no such file is found
     */
    public function render($pTemplateFile)
    {
        if(file_exists(__DIR__ . '\..' . $this->templateDir . $pTemplateFile))
            include __DIR__ . '\..' . $this->templateDir . $pTemplateFile;
        else
            throw new Exception('No template file named "' . $pTemplateFile . '" in directory ' . $this->templateDir);
    }
}