<?php
/*
 * Our custom post type
 */
class LexiconWpTest1CustomPost
{
    /*
     * The very basic custom post type for wordpress
     * @category    Custom_post
     * @package     Custom_post_test
     * @subpackage  Test
     * @copyright   Copyright (c) 2015 Tommy Morberg
     * @license     Test
     * @version     0.2
     * @link        http://google.com
     * @since       Class available since 0.1
     * @deprecated  Never
     */
    private $labels;
    private $supports;
    private $rewrite;
    
    /*
     * Construct function
     */
    public function __construct()
    {
        $this->setLabels();
        $this->setRewrite();
        $this->setSupports();
    }
    
    /*
     * Basic init function that are called from outside the class
     */
    public function init()
    {
        add_action('init', array($this, 'basicSetup'));
    }
    
    /*
     * Setup function called from the init function,
     * uses variables in class to register the custom post type
     */
    public function basicSetup()
    {
        register_post_type('customPost', array(
            'labels'        => $this->getLabels(),
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => $this->getRewrite(),
            'supports'      => $this->getSupports()
            )
        );
    }
    
    /*
     * Setting class variable $labels
     */
    private function setLabels()
    {
        $this->labels = array('name'            => __('Custom posts'),
                              'singular_name'   => __('Custom post')
        );
    }
    
    /*
     * Getting class variable $labels
     * @return array
     */
    private function getLabels()
    {
        return $this->labels;
    }
    
    /*
     * Setting class variable $rewrite
     */
    private function setRewrite()
    {
        $this->rewrite = array('slug', 'customPosts');
    }
    
    /*
     * Getting class variable $labels
     * @return array
     */
    private function getRewrite()
    {
        return $this->rewrite;
    }
    
    /*
     * Setting class variable $supports
     */
    private function setSupports()
    {
        $this->supports = array('title', 'editor', 'thumbnail');
    }
    
    /*
     * Getting class variable $labels
     * @return array
     */
    private function getSupports()
    {
        return $this->supports;
    }
}