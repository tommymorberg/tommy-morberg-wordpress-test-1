<?php
/*
 * Widget for our custom post type
 */
class LexiconWpTest1Widget extends WP_Widget
{
    /*
     * The very basic widget for showing custom post types
     * @category    Widget
     * @package     Widget_test
     * @subpackage  Test
     * @copyright   Copyright (c) 2015 Tommy Morberg
     * @license     Test
     * @version     0.2
     * @link        http://google.com
     * @since       Class available since 0.1
     * @deprecated  Never
     */
    
    /*
     * Construct function
     * Adding a custom css file
     */
    function __construct()
    {
        parent::__construct('customposttypewidget', 'Custom post type widget',
            array( 'description' => 'This is a test widget for custom post type')
        );
        wp_register_style( 'widgetStyles', '/wp-content/plugins/test/css/test.css', array(), false, 'screen');
        wp_enqueue_style( 'widgetStyles');
    }
    
    /*
     * Collecting custom posts and showing the result
     * @argument $pArgs
     * @argument $pInstance
     */
    public function widget($pArgs, $pInstance)
    {
        $title = apply_filters( 'widget_title', $pInstance['title'] );
        $queryArgs = array('post_type'      => 'custompost',
                           'post_status'    => 'publish'
        );
        $dataForTemplate = array();
        $query = new WP_Query($queryArgs);
        $index = 0;
        if($query->have_posts()){
            while($query->have_posts())
            {
                $query->the_post();
                $content = get_the_content();
                if(strlen($content) > 30)
                    $content = substr($content, 0, 30) . '...';
                $dataForTemplate[] = array('title'      => get_the_title(),
                                           'content'    => $content,
                                           'image'      => get_the_post_thumbnail()
                );
                ++$index;
                if($index >= $pInstance['nrPosts'])
                    break;
            }
        }
        
        $view = new View();
        $view->widgetTitle  = (empty($title) ? 'Custom posts' : $title);
        $view->beforeWidget = $pArgs['before_widget'];
        $view->afterWidget  = $pArgs['after_widget'];
        $view->templateData = $dataForTemplate;
        $view->render('test.php');
    }
    
    /*
     * Displaying the form when looking at the widget in wordpress-appearance-widget
     * @argument $pInstance
     */
    public function form($pInstance)
    {
        if(isset($pInstance['title']))
            $title = $pInstance['title'];
        else
            $title = 'New title';
        if(isset($pInstance['nrPosts']))
            $number = $pInstance['nrPosts'];
        else
            $number = 10;
        ?>
        <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
        name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
        value="<?php echo esc_attr( $title ); ?>" />
        
        <label for="<?php echo $this->get_field_id( 'nrPosts' ); ?>"><?php _e( 'Max posts shown:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'nrPosts' ); ?>"
        name="<?php echo $this->get_field_name( 'nrPosts' ); ?>" type="number"
        value="<?php echo esc_attr( $number ); ?>" />
        </p>
        <?php 
    }
    
    /*
     * Updating database when clicking save on widget
     * @argument $pNewInstance
     * @argument $pOldInstance
     * @return array
     */
    public function update($pNewInstance, $pOldInstance)
    {
        $instance = array();
        $instance['title']      = (!empty($pNewInstance['title'])) ? strip_tags($pNewInstance['title']) : '';
        $instance['nrPosts']    = (!empty($pNewInstance['nrPosts'])) ? strip_tags($pNewInstance['nrPosts']) : '';
        return $instance;
    }
}

/*
 * Registering the widget in wordpress
 */
function registerWidget()
{
    register_widget( 'LexiconWpTest1Widget' );
}

add_action( 'widgets_init', 'registerWidget' );
?>